<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Service\CarrierService;
use App\Service\PackGroup\PackGroup;
use App\Service\TransCompany;
use PHPUnit\Framework\TestCase;

class CarrierServiceTest extends TestCase
{
    public function testCalculatePriceTransCompany()
    {
        $transCompany = new TransCompany('TransCompany');
        $packGroup = new PackGroup('PackGroup');
        $carrierService = new CarrierService($transCompany, $packGroup);

        $price = $carrierService->calculatePrice('transcompany', 5);

        $this->assertEquals(20, $price);
    }
}
