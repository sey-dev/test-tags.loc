import { createApp } from "vue";
import calculatePriceApp from "./calculate-price-app/index.vue";

createApp(calculatePriceApp).mount('#calculate-price-app');
