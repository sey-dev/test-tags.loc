<?php

declare(strict_types=1);

namespace App\Service\Parser\TextParser;

class TextParserService
{
    public function parseKeys(string $text): array
    {
        $result = [];

        $pattern = '/(?:one:|two:|three:)(.*?)(?=(one:|two:|three:|$))/s';

        preg_match_all($pattern, $text, $matches);

        foreach ($matches[0] as $index => $match) {
            $key = strtok($match, ':');
            $value = trim($matches[1][$index]);
            $result[$key] = $value;
        }
        return $result;
    }
}
