<?php

declare(strict_types=1);

namespace App\Service\Parser\TagParser;

class TagParserService
{
    public function parseTags(string $text): array
    {
        $tagData = [];
        $tagDescriptions = [];

        // Регулярний вираз для пошуку тегів і описів
        $pattern = '/\[([^]]+)\](.*?)\[\/\1\]/';

        // Знаходимо всі входження відповідно до регулярного виразу
        preg_match_all($pattern, $text, $matches, PREG_SET_ORDER);

        // Перебираємо знайдені входження
        foreach ($matches as $match) {
            $tagName = $match[1];
            $tagData[$tagName] = $match[2];

            // Перевіряємо, чи є опис тега
            if (preg_match('/description="([^"]+)"/', $tagName, $descriptionMatch)) {
                $tagDescriptions[$tagName] = $descriptionMatch[1];
            }
        }

        return [
            'tagData' => $tagData,
            'tagDescriptions' => $tagDescriptions
        ];
    }
}
