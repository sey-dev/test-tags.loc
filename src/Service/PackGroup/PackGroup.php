<?php

declare(strict_types=1);

namespace App\Service\PackGroup;

use App\Service\Carrier;

class PackGroup extends Carrier
{
    public function calculateShippingCost($weight): float
    {
        return $weight;
    }
}
