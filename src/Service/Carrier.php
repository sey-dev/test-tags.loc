<?php

declare(strict_types=1);

namespace App\Service;

abstract class Carrier
{
    protected string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    abstract public function calculateShippingCost($weight): float;

    public function getName(): string
    {
        return $this->name;
    }
}
