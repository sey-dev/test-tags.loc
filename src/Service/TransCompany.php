<?php

declare(strict_types=1);

namespace App\Service;

class TransCompany extends Carrier
{
    public function calculateShippingCost($weight): float
    {
        if ($weight <= 10) {
            return 20;
        } else {
            return 100;
        }
    }
}
