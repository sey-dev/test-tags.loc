<?php

declare(strict_types=1);

namespace App\Service;

use App\Service\PackGroup\PackGroup;

class CarrierService
{
    private TransCompany $transCompany;
    private PackGroup $packGroup;

    public function __construct(TransCompany $transCompany, PackGroup $packGroup)
    {
        $this->transCompany = $transCompany;
        $this->packGroup = $packGroup;
    }

    public function calculatePrice($carrierSlug, $weight)
    {
        if ($carrierSlug === 'transcompany') {
            return $this->transCompany->calculateShippingCost($weight);
        } elseif ($carrierSlug === 'packgroup') {
            return $this->packGroup->calculateShippingCost($weight);
        } else {
            throw new \Exception('Unknown carrier');
        }
    }
}
