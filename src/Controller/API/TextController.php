<?php

declare(strict_types=1);

namespace App\Controller\API;

use App\Service\Parser\TextParser\TextParserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class TextController extends AbstractController
{
    #[Route('/api/parse-keys', name: 'app_api_parse_keys', methods: 'POST' )]
    public function __invoke(Request $request, TextParserService $textParserService)
    {
        $text = $request->getContent();
        $result = $textParserService->parseKeys($text);

        return new JsonResponse($result);
    }
}
