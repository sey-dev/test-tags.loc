<?php

declare(strict_types=1);

namespace App\Controller\API\Calculate;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class CalculatePriceController
{
    #[Route('/api/calculate-price', name: 'app_api_calculate_price', methods: 'POST')]
    public function __invoke(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $weight = $data['weight'];
        $carrierSlug = $data['carrier_slug'];

        $price = $this->carrierService->calculatePrice($carrierSlug, $weight);

        return $this->json(['price' => $price]);
    }
}
