<?php

declare(strict_types=1);

namespace App\Controller\API\Calculate;

use App\Service\CarrierService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ApiController extends AbstractController
{
    private $carrierService;

    public function __construct(CarrierService $carrierService)
    {
        $this->carrierService = $carrierService;
    }

    #[Route('/api/calculate', name: 'app_api_test', methods: 'GET')]
    public function __invoke(): Response
    {
        return $this->render('api/index.html.twig');
    }
}
