<?php

declare(strict_types=1);

namespace App\Controller\API;

use App\Service\Parser\TagParser\TagParserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class TagController extends AbstractController
{
    #[Route('/api/parse-tags', name: 'app_api_parse_tags', methods: 'POST' )]
    public function __invoke(Request $request, TagParserService $tagParserService)
    {
        $text = $request->getContent();
        $result = $tagParserService->parseTags($text);

        return new JsonResponse($result);
    }
}
